<?php 
    require 'requete.php';
   if(isset($_POST['submit'])){    
        require 'db.php';
        $nom=htmlspecialchars( $_POST['nomU']);
        $prenom=htmlspecialchars($_POST['PrenomU']);
        $matricule=htmlspecialchars($_POST['MatriculeU']);
        $gender=htmlspecialchars($_POST["gender"]);
        $Code_cl=htmlspecialchars($_POST['i']);

            if((!empty($nom)) && (!empty($prenom)) && (!empty($matricule)) && (!empty($gender))&& (!empty($Code_cl)) ){

              
                //verifier matricule

                $v_matricule=$pdo->prepare("SELECT Matricule FROM ETUDIANT WHERE  Matricule=? ") ;
                $v_matricule->execute([$matricule]) ;
                $v=$v_matricule->fetch();
                if ($v){
                  $errormatricule="ce matricule existe deja";
    
                }else { 
                   //prendre l'id de la classe concernee
                   $id =$pdo->prepare("SELECT Code_cl FROM Class WHERE  Intitule=? ") ;
                   $id->execute( [ $Code_cl]);
                   //enregister l etudiant

                       while($d=$id->fetch()){  $id1=$d['Code_cl'];
                           $req= $pdo->prepare("INSERT INTO ETUDIANT SET Nom=?, Prenom=?,Matricule=?,Sexe=? ,Code_cl=?");
                           $req->execute([$nom,$prenom,$matricule,$gender,$id1]);
                           $succesmessage='INSCRIPTION EFFECTUE AVEC SUCCES';
                           }
           
                     // header('Location:index.php');
             
                }
            
                
            }else{
             
                $errormessage="Veuilez remplir tous les champs...";
            }
    }
    ?>

<!DOCTYPE html>
<!-- Created By CodingNepal -->
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <!-- Somehow I got an error, so I comment the title, just uncomment to show -->
    <!-- <title>Login & Signup Form | CodingNepal</title> -->
    <link rel="stylesheet" href="login.css">
     <!-- Bootstrap CSS -->
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <div class="wrapper">
      <div class="title-text">
        <div class="title login">Formulaire
     <div>   <?php if (isset($errormessage))  {?><h6 class="alert alert-danger" role="alert"><?= $errormessage ?>  </h6> <?}?>
        <?php if (isset($succesmessage))  {?><h6 class="alert alert-success" role="alert" ><?= $succesmessage ?>  </h6> <?}?>
        </div> 
        </div>
        <div class="title signup">Inscription
        </div>
   
      </div>
    <div class="form-container">
        <div class="slide-controls">
          <input type="radio" name="slide" id="login" checked>
          <input type="radio" name="slide" id="signup">
          <label for="login" class="slide login">se connecter</label>
          <label for="signup" class="slide signup">s'inscrire</label>
          <div class="slider-tab">
        </div>
    </div>

    <div class="form-inner">
          <form action="#" class="login">
              <div class="field">
                <input class="input--style-1" type="text" placeholder="Nom" name="nomU">
              </div>

              <div class="field">
                <input type="text" placeholder="Matricule" required>
                
                
              </div>

              <div class="pass-link"><a href="#">matricule oublie?</a></div>
              <div class="field btn">
              <div class="btn-layer">
          </div>

<input type="submit" value="Login">
            </div>
<div class="signup-link">
pas encore inscrit? <a href="">cliquez ici</a></div>
</form>

<form method="POST"  class="signup">
            <div class="field">
            <input class="input--style-1" type="text" placeholder="Nom" name="nomU">
            </div>
<div class="field">
<input class="input--style-1" type="text" placeholder="Prenom" name="PrenomU">
                       
            </div>
            <div class="field">
              <input type="text" placeholder="MatriculeU" name="MatriculeU" required>
              <?php if (isset($errormatricule))  {?><div style="color:red;size:120px"><?= $errormatricule ?>  </div> <?}?><p>
            </div>
            <br><br>
                 <div class="rs-select2 js-select-simple select--no-search">
                      <label class="mr-sm-2" for="inlineFormCustomSelect">Preference</label>
                           <select name="i" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                               <option  selected>Choose...</option>
                               <?php while($class=$req2->fetch()){ $c=$class['Code_cl'] ?>
                               <option ><?php echo $class["Intitule"] ?></option>
                               <?php }?> 
                           </select> 
                  </div>
                                
            
            <div class="field">
            <div class="input-group">
                                    <div class="rs-select2 js-select-simple select--no-search">
                                        <select name="gender">
                                            <option disabled="disabled" selected="selected">Genre</option>
                                            <option>masculin</option>
                                            <option>feminin</option>
                                            <option>autre</option>
                                        </select>
                                        <div class="select-dropdown"></div>
                                    </div>
                                </div>

                </div>
<div class="field btn">

              <div class="btn-layer">
</div>
<input name='submit' type="submit" value="S'inscrire">
            </div>
</form>
</div>
</div>
</div>
<script>
      const loginText = document.querySelector(".title-text .login");
      const loginForm = document.querySelector("form.login");
      const loginBtn = document.querySelector("label.login");
      const signupBtn = document.querySelector("label.signup");
      const signupLink = document.querySelector("form .signup-link a");
      signupBtn.onclick = (()=>{
        loginForm.style.marginLeft = "-50%";
        loginText.style.marginLeft = "-50%";
      });
      loginBtn.onclick = (()=>{
        loginForm.style.marginLeft = "0%";
        loginText.style.marginLeft = "0%";
      });
      signupLink.onclick = (()=>{
        signupBtn.click();
        return false;
      });
    </script>

     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

  </body>
</html>
