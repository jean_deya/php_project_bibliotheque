<!DOCTYPE html>
<!-- Created By CodingNepal -->
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <!-- Somehow I got an error, so I comment the title, just uncomment to show -->
    <!-- <title>Dropdown Menu Bar</title> -->
    <link rel="stylesheet" href="acceuil.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  </head>
  <body>
    <nav>
      <label class="logo">Ma biblio</label>
      <ul>
<li><a class="active" href="#">Acceuil</a></li>
<li><a href="#">Mon compte
          <i class="fas fa-caret-down"></i>
        </a>
          <ul>
<li><a href="login.php">se connecter</a></li>
<li><a href="#">Espace Etudiants</a></li>

</ul>
</li>
<li><a href="#">Literrature
          <i class="fas fa-caret-down"></i>
        </a>
          <ul>
<li><a href="#">Auteurs connus</a></li>
<li><a href="#">Dernieres sorties</a></li>
<li><a href="#">Auteurs Celebres
              <i class="fas fa-caret-right"></i>
            </a>
              <ul>
<li><a href="#">Links</a></li>
<li><a href="#">Works</a></li>
<li><a href="#">Status</a></li>
</ul>
</li>
</ul>
</li>
<li><a href="#">Contact</a></li>
<li><a href="#">Feedback</a></li>
</ul>
</nav>
    <section>
    <div class="card" style="width: 18rem;">
      <img src="..." class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">Card title</h5>
        <p class="card-text">578 classiques de la littérature francophone, disponibles gratuitement</p>
        <a href="#" class="btn btn-primary">Commencer l'experience</a>
     </div>
    </div>
    </section>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
  </body>
</html>
