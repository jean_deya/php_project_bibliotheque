<?php 
  
    
      
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Au Register Forms by Colorlib</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="style.css" />
    <!-- Main CSS-->
    <link  rel="stylesheet" href="main.css" >
</head>

<body>
    <div class="page-wrapper bg-blue p-t-100 p-b-100 font-robo">
        <div class="wrapper wrapper--w680">
            <div class="card card-1">
                <div class="card-heading"></div>
                <div class="card-body">
                    <h2 class="title">FICHE D INSCRIPTION</h2>
                    <?php if (isset($errormessage))  {?><p style="color:red"><?= $errormessage ?>  </p> <?}?>
                    <?php if (isset($succesmessage))  {?><p style="color:green"><?= $succesmessage ?>  </p> <?}?>
                    <form  method="POST">
                        <div class="input-group">
                            <input class="input--style-1" type="text" placeholder="Nom" name="nomU">
                            <?php if (empty($_POST['nomU'])){
                $errors['nomU']="Vous n'avez pas rentr2 de nom d'utilisateur";
                ?>
                <div class="error"><?php echo "Vous n'avez pas rentré de nom d'utilisateur";?></div>
            <?php  }?>
                        </div>
                        <div class="input-group">
                            <input class="input--style-1" type="text" placeholder="Prenom" name="PrenomU">
                       
                            <?php  if (empty($_POST['PrenomU'])){
                $errors['PrenomU']="Vous n'avez pas rentr2 de nom d'utilisateur";
                ?>
                <div class="error"><?php echo "Vous n'avez pas rentré de prenom";?></div>
           <?php      }  ?> </div>
           <div class="col-2">
                <div class="input-group">
                    <div class="rs-select2 js-select-simple select--no-search">
                        <div class="col-auto my-1">
                            <label class="mr-sm-2" for="inlineFormCustomSelect">Preference</label>
                           
                                <select name="i" class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                    <option  selected>Choose...</option>
                                    <?php while($class=$req2->fetch()){ $c=$class['Code_cl'] ?>
                                    <option ><?php echo $class["Intitule"] ?></option>
                                    <?php }?> 
                                </select>
                           
                                    
                        </div>
                    </div>
                                      
                </div>
                                   
                                  
             </div>
                                
                            

                            <div class="form-row align-items-center">
                  
                        <div class="row row-space">
                           
                            <div class="col-2">
                                <div class="input-group">
                                    <div class="rs-select2 js-select-simple select--no-search">
                                        <select name="gender">
                                            <option disabled="disabled" selected="selected">Genre</option>
                                            <option>masculin</option>
                                            <option>feminin</option>
                                            <option>autre</option>
                                        </select>
                                        <div class="select-dropdown"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-2">
                                <div class="input-group">
                                    <input class="input--style-1" type="text" placeholder="matricule" name="MatriculeU">
                                </div>
                            </div>
                        </div>
                        <div class="p-t-20">
                            <button class="btn btn--radius btn--green" name='submit' type="submit">Envoyer</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>
     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->